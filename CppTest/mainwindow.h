#pragma once
#include "timer.h"
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void setTimer(timer & timerToConnect);
    ~MainWindow();
private:
    Ui::MainWindow *ui;
};

