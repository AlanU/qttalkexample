#pragma once
#include <QObject>
#include <QTimer>
class timer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int count READ getCount WRITE setCount NOTIFY countChanged)
public:
    timer(QObject *parent = 0);
    void setCount(int newCount) { count = newCount; emit countChanged(newCount);}
    int getCount(){return count;}
public slots:
    void update();
signals:
    void countChanged(int) ;
private:
    int count;
    QTimer time;
};

