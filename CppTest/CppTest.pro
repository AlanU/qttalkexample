#-------------------------------------------------
#
# Project created by QtCreator 2015-08-05T01:22:16
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CppTest
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    timer.cpp


HEADERS  += mainwindow.h \
    timer.h

FORMS    += mainwindow.ui
