#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "timer.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setTimer(timer & timerToConnect)
{
    QObject::connect(&timerToConnect,SIGNAL(countChanged(int)),ui->countLable,SLOT(setNum(int)));

}

