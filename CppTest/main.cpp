#include "mainwindow.h"
#include <QApplication>
#include "timer.h"
#include <QMetaProperty>
#include <QDebug>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    timer newTimer(NULL);
    newTimer.setProperty("count",1);
    newTimer.setCount(2);

    QObject *object = &newTimer;
    const QMetaObject *metaobject = object->metaObject();
    int count = metaobject->propertyCount();
    for (int i=0; i<count; ++i) {
        QMetaProperty metaproperty = metaobject->property(i);
        const char *name = metaproperty.name();
        QVariant value = object->property(name);
        qDebug()<<metaobject->className()<<"name of property"<<name<<endl;
    }
    w.setTimer(newTimer);
    w.show();

    return a.exec();//returns on quit
}
