#include "timer.h"
#include <QTimer>
#include <QObject>
timer::timer(QObject *parent) : QObject(parent)
{
    QObject::connect(&time,SIGNAL(timeout()),this,SLOT(update()));
    time.start(1000);
}

void timer::update()
{

    setCount(count+1);
}

